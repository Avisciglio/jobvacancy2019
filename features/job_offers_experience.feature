Feature: Experience field
    In order to get the correct job applys
    I want to set a job experience requirement
Background: Offerer logged in
    Given I am logged in as job offerer

  Scenario: Experience seted to 3 years
    Given I access the new offer page
    When I fill the title with "Programmer vacancy"
    When I fill the experience with 3
		And confirm the new offer    
    Then I should see 3 in Experience

  Scenario: Experience seted to 3 years
    Given I access the new offer page
    When I fill the title with "Programmer vacancy"
    When I fill the experience with 0
		And confirm the new offer    
    Then I should see "Not specified" in My Offers
