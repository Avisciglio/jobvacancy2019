class JobOffer
  include ActiveModel::Validations

  attr_accessor :id, :user, :user_id, :title,
                :location, :description, :is_active,
                :updated_on, :created_on, :experience

  validates :title, presence: true
  validates :experience, presence: true, numericality: { only_integer: true,
                                                         greater_than_or_equal_to: 0,
                                                         less_than_or_equal_to: 50 }

  def initialize(data = {})
    @id = data[:id]
    @title = data[:title]
    @location = data[:location]
    @description = data[:description]
    @is_active = data[:is_active]
    @updated_on = data[:updated_on]
    @created_on = data[:created_on]
    @user_id = data[:user_id]
    @experience = data[:experience]
  end

  def owner
    user
  end

  def view_experience
    if @experience.zero?
      'Not specified'
    else
      @experience
    end
  end

  def owner=(a_user)
    self.user = a_user
  end

  def activate
    self.is_active = true
  end

  def deactivate
    self.is_active = false
  end

  def old_offer?
    (Date.today - updated_on) >= 30
  end
end
